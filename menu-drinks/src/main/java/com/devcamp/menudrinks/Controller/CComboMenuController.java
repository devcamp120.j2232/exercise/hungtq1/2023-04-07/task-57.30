package com.devcamp.menudrinks.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.menudrinks.models.CMenu;
import com.devcamp.menudrinks.service.CMenuService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class CComboMenuController {
  @Autowired
  private CMenuService cMenuService;

  @GetMapping("/combo-menu")
  public ArrayList<CMenu> getComboMenu() {
    return cMenuService.getComboMenu();
  }

}
